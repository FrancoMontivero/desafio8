import express from 'express';

import cors from './middlewares/cors';
import router from './routes/index';
import morgan from './middlewares/morgan'
import multer from './middlewares/multer';

import { PORT } from './config';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended : true }));
app.use(multer.array('any'));

app.use(cors);
app.use(morgan);

app.use(router);
app.use((err: any, req: any, res: any, next: any) => {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    res.json({'error': err.message});
});

app.set('port', PORT);

export default app;
