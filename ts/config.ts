import dotenv from 'dotenv';
import path from 'path';

dotenv.config({
    path: path.resolve(__dirname, '../', process.env.NODE_ENV + '.env')
});

export let PORT: number = process.env.PORT ? parseInt(process.env.PORT) : 8080;
export let ORIGIN_CLIENT: string = process.env.ORIGIN_CLIENT || 'http://localhost:8081';
