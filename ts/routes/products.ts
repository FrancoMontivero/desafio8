import express from 'express';

const routerProducts = express.Router();

let array: any[] = [];

routerProducts.get('/listar', (req, res) => {
	if (array.length) res.json(array);
	else res.json({error: "No hay productos cargados"});
})

routerProducts.get('/listar/:id', (req, res, next) => {
	try {
		const id: number = parseInt(req.params.id);
		if(isNaN(id)) res.json({error: "El id del produto no es un numero"})
		else {
			let index = array.findIndex(e => e.id === id);
			if(index === -1) res.json({error: "Producto no encontrado"})
			else res.json(array[index])
		}
	}
	catch (err) {
		next(err);
	}
})

routerProducts.post('/guardar', (req, res) => {
	const {
		title,
		price,
		thumbnail
	} = req.body;
	
	if(!(title && price && thumbnail)) {
		res.json({error: "Hay parametros vacios o indefinidos"});
	}
	else {
		const newProduct = {id: (array[array.length -1]?.id || 0) + 1, title, price, thumbnail};
		array.push(newProduct);
		res.json(newProduct);
	}

})

export default routerProducts;