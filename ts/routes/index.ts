import express from 'express';
import routerProducts from './products';

const router = express.Router();

router.use('/api/productos', routerProducts);


export default router;
