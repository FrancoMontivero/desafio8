"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("./middlewares/cors"));
var index_1 = __importDefault(require("./routes/index"));
var morgan_1 = __importDefault(require("./middlewares/morgan"));
var multer_1 = __importDefault(require("./middlewares/multer"));
var config_1 = require("./config");
var app = express_1.default();
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(multer_1.default.array('any'));
app.use(cors_1.default);
app.use(morgan_1.default);
app.use(index_1.default);
app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    res.json({ 'error': err.message });
});
app.set('port', config_1.PORT);
exports.default = app;
